#!/bin/bash

#Server de interfaz
cd iotec_webplatform
cd nodejs_frontend
cd src
nohup node server.js &

#Server para recibir imagenes
cd images
cd zmq_server
nohup python FitoMaiServer.py &

cd ..
cd ..
cd ..
cd ..
cd ..

#Server de Tokens
cd iotec_webtoken
cd nodejs_webtoken
nohup node server.js &

cd ..
cd ..

#Server de rest_api
cd semanai_rest_api
cd src
nohup node server.js &

cd ~