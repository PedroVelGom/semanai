import signal
import zmq
import six
import base64
from pymongo import MongoClient
import gridfs
import requests
import json
from time import sleep

#from google.cloud import storage
#from google.cloud import datastore

import datetime

if __name__ == '__main__':

 ctx = zmq.Context()
 socket = ctx.socket(zmq.REP)
 socket.bind("tcp://*:8184")
 print(" ")
 print("--------------Fitotron SERVER READY -------------") 

while True:

    message = socket.recv()

    if message.startswith('{'):
     # It's a string !!
     myparams = json.loads(message)

     
     code_device = myparams['code_device']
      
     fecha = myparams['fecha']
     values = json.dumps(myparams['values'])
     values1 = json.loads(values)
     print values
     client = MongoClient("mongodb://35.185.213.109:27017/")
     db = client['iotec-petervel']
     result = db.logs.insert_one({"device_id":myparams['code_device'],"Temp" : values1['temp'], "Lat" : values1['LAT'],"Lon" : values1['LONG']})
     re = "inserted ok"
     socket.send(re)

    elif message.startswith('5'):
        nombre = message + ".jpg"
        socket.send("Ok")
    else:
     img_recv = (message)
     img_result = open(nombre, 'w')
     img_result.write(img_recv)
     print("Image  received")


     #client = storage.Client()
     #bucket = client.get_bucket('fitotron-api.appspot.com')
     #print  "--- bucket name ---"
     #print bucket

     today = '%s' % datetime.datetime.now()
     today = today.replace(' ', '_')
     today = today.replace(':', '_')
     today = today.replace('.', '_')
     print today

     #blob = bucket.get_blob('imagen.jpg')
     #urlblob = "mr3_%s.jpg" % today
     #blob = bucket.blob(urlblob)
     #print blob
     #blob.upload_from_filename('./imagen.jpg')
     #url = blob.public_url
     #print('Blob '+ url)

     # Get the feed



     re = "image received ok..."
     socket.send(re)
