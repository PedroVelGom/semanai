// get the packages we need

var express = require('express');
var cors = require('cors');

var app = express();

app.use(cors());

var bodyParser = require('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');

var jwt = require('jsonwebtoken');
var config = require('./config');


var User = require('./app/models/user');
var Device = require('./app/models/device');

// configuration

var port = process.env.PORT || 8183;
mongoose.connect(config.database);
app.set('superSecret', config.secret);

// use body parser so we can get info from POST
// and/or URL parameters
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

//
// routes
//

app.get('/', function (req, res) {
	res.send('Que hongo, la API esta en http://35.185.213.109:' + port + '/api');
});

// add demo user

app.get('/setup', function (req, res) {
	// create a sample user
	var nick = new User({
		name : 'peter',
		password: 'peter',
		admin: true
	});

	// save the sample user

	nick.save(function(err) {
		if (err) throw err;

		console.log('Chido! estas adentro');
		res.json({success: true});	
	});
});

// api routes

// get an instance of the router for api routes
var apiRoutes = express.Router();

// route to authenticate a user

apiRoutes.post('/authenticate', function (req, res) {
 
 	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    //res.setHeader('Access-Control-Allow-Credentials', true); // If needed

	//find the user
	User.findOne({name: req.body.name}, function (err, user) 
	{
		if (err) throw err;
	
		if (!user) {
			res.json({success: false, message: 
				'Vuelvelo a intentar carnal, algo hiciste mal'});
		}
		else if (user) {
			// check if password matches
			if (user.password != req.body.password) {
				res.json({success : false, message : 
					'Vuelvelo a intentar carnal, checha tu contraseña'});
			}
			else {
				// user and password is right
				var token = jwt.sign(user, app.get('superSecret'), {
					expiresIn: 60 * 60
				});

				// return the information including token as JSON

				res.json({
					success : true,
					message : 'aqui ta tu token !',
					token : token
				});
				
			}
		}

		
	});
});

// route to middleware to verify a token

apiRoutes.use(function (req, res, next) {
	// check header or url parameters or post parameters for token
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    //res.setHeader('Access-Control-Allow-Credentials', true); // If needed


	var token = req.body.token ||
	 			req.query.token ||
	 			req.headers['x-access-token'];
	// decode token
	if (token)	{
		// verifies secret and checks up
		jwt.verify(token, app.get('superSecret'), function (err, decoded) {
			if (err) {
				return res.json({success: false, message : 'Ese no es el token que te di we 7u7' });

			} else {
				// is everything is good, save to request for use in other routes
				req.decoded = decoded;
				next();
			}
		});
	}else {
		// if there is not token, return an error

		return res.status(403).send( {
			success: false,
			message: 'onta el token?'
		});
	}
});


// route to show a random message

apiRoutes.get('/', function (req, res) {
	res.json({message: 'wachate mi hermosa API'});
});

// route to return all users

apiRoutes.get('/users', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	User.find({}, function(err, users) {
		res.json(users);
	});
});



// route to return all devices

apiRoutes.get('/devices', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	Device.find({}, function(err, devices) {
		res.json(devices);
	});
});

apiRoutes.get('/devices/:device_id', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
	// body...
 	Device.findById(req.params.device_id, function(err, device)
 	{
 		if (err)
 			res.send(err);
 		res.json(device);

 	});
  
});

 // create a new device accessed at POST
  // http://35.185.213.109:8082/api/devices
apiRoutes.post('/devices', function (req, res) {
 	var device = new Device();
 	device.name = req.body.name;
  	device.code = req.body.code;
  	device.description = req.body.description;
 	// save the bear and check for errors
 	device.save(function (err) {
 		// body...
 		if (err)
 			res.send(err);
 		res.json({ message: 'tu chuche ha sido creado !'});
 	});

 	// body...
 });


 // create a new device accessed at PUT
  // http://35.185.213.109:8082/api/devices
apiRoutes.put('/devices/:device_id', function (req, res) {
 	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	

 	var device = new Device();
 	
 	
 	Device.findById(req.params.device_id, function(err, device) {
 		if (err)
 			res.send(err);

 		// update the devices info
 		device.name = req.body.name;
	    device.code = req.body.code;
	    device.description = req.body.description;
 		// save the bear

 		device.save(function (err) {
 			if (err)
 				res.send(err);
 			res.json({message: 'la wea se actualizo !'});
 		});
 	});
 	// body...
 });

// delete the device with this id 
 // accessed at DELETE
 // http://35.185.213.109:8081/api/devices/:device_id
apiRoutes.delete('/devices/:device_id', function (req, res) {
   res.setHeader('Access-Control-Allow-Origin', '*');
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
   	
   Device.remove({
   	_id : req.params.device_id
   }, function (err, device) {
   	if (err)
   		res.send(err);
   	res.json({message: 'ayo popo!'});
   });

 });


 
 

//var cors = require('cors');

// use it before all route definitions
//app.use(cors({origin: 'http://35.185.213.109:8083'}));
// apply the routes to our application with the prefix /api
app.use('/api', apiRoutes);
app.disable('etag');


// start the server
app.listen(port);
console.log('Quien anda ahi o<o : ' + port);
